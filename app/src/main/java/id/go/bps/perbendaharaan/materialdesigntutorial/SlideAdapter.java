package id.go.bps.perbendaharaan.materialdesigntutorial;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

/**
 * Created by viktor.suwiyanto on 21/06/2016.
 */
public class SlideAdapter extends FragmentPagerAdapter {

    private Context context;
    private String[] titles = {"HOME","GALERY","PROFILE"};
    int[] icons = new int[]{R.mipmap.ic_home_white_48dp,R.mipmap.ic_collections_white_48dp,R.mipmap.ic_account_box_white_48dp};
    private int heightIcon;

    public SlideAdapter(FragmentManager fm,Context context) {
        super(fm);
        this.context = context;
        double scale = context.getResources().getDisplayMetrics().density;
        heightIcon = (int) (24*scale+0.5f);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if(position == 0){
            fragment = new RecyclerViewFragment();
        }else if (position == 1){
            fragment = new GaleryFragment();
        }else if (position == 2){
            fragment = new ProfileFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable drawable = context.getResources().getDrawable(icons[position]);
        drawable.setBounds(0,0,heightIcon,heightIcon);

        ImageSpan imageSpan = new ImageSpan(drawable);

        SpannableString spannableString = new SpannableString(" ");
        spannableString.setSpan(imageSpan,0,spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //return super.getPageTitle(position);
        return spannableString;
    }
}
