package id.go.bps.perbendaharaan.materialdesigntutorial;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class GaleryFragment extends Fragment {

    protected RecyclerView mRecyclerView;
    protected GridAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<GridModelItem> itemList;


    public GaleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.recycler_view_frag, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(getActivity(),3);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //mengisi listItem
        itemList = new ArrayList<GridModelItem>();
        itemList.add(new GridModelItem(R.drawable.worker_1,"Pekerja 1"));
        itemList.add(new GridModelItem(R.drawable.worker_2,"Pekerja 2"));
        itemList.add(new GridModelItem(R.drawable.worker_3,"Pekerja 3"));
        itemList.add(new GridModelItem(R.drawable.worker_4,"Pekerja 4"));


        mAdapter = new GridAdapter(itemList);
        mRecyclerView.setAdapter(mAdapter);

        return  rootView;
    }

}
