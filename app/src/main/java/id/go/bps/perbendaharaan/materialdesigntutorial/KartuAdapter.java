package id.go.bps.perbendaharaan.materialdesigntutorial;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class KartuAdapter extends RecyclerView.Adapter<KartuAdapter.ViewHolders> {

    private static final String TAG = "KartuAdapter";

    private List<KartuModel> itemList;

    public KartuAdapter (List<KartuModel> itemList) {
        this.itemList = itemList;
    }

    public static class ViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView judul;
        public TextView deskripsi;
        public ImageView gambar;

        public ViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            judul = (TextView)itemView.findViewById(R.id.judul_kartu);
            deskripsi = (TextView)itemView.findViewById(R.id.desk_kartu);
            gambar = (ImageView)itemView.findViewById(R.id.gambar);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "Element " + getPosition() + " clicked.");
        }
    }

    @Override
    public ViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profil_view, parent, false);
        ViewHolders rcv = new ViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ViewHolders holder, int position) {
        holder.judul.setText(itemList.get(position).getJudul());
        holder.deskripsi.setText(itemList.get(position).getDeskripsi());
        holder.gambar.setImageResource(itemList.get(position).getGambar());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}