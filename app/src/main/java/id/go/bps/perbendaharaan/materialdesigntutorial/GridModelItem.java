package id.go.bps.perbendaharaan.materialdesigntutorial;

/**
 * Created by viktor.suwiyanto on 23/06/2016.
 */
public class GridModelItem {
    private String deskripsi;
    private int mThumbnail;

    public GridModelItem(int mThumbnail, String deskripsi) {
        this.mThumbnail = mThumbnail;
        this.deskripsi = deskripsi;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(int mThumbnail) {
        this.mThumbnail = mThumbnail;
    }
}
