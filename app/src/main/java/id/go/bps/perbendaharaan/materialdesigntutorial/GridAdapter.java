package id.go.bps.perbendaharaan.materialdesigntutorial;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolders> {

    private static final String TAG = "GridAdapter";

    private List<GridModelItem> itemList;

    public GridAdapter (List<GridModelItem> itemList) {
        this.itemList = itemList;
    }

    public static class ViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView deskripsi;
        public ImageView img_thumbnail;

        public ViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            deskripsi = (TextView)itemView.findViewById(R.id.tv_android);
            img_thumbnail = (ImageView)itemView.findViewById(R.id.img_thumbnail);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "Element " + getPosition() + " clicked.");
        }
    }

    @Override
    public ViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        ViewHolders rcv = new ViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ViewHolders holder, int position) {
        holder.deskripsi.setText(itemList.get(position).getDeskripsi());
        holder.img_thumbnail.setImageResource(itemList.get(position).getThumbnail());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}