package id.go.bps.perbendaharaan.materialdesigntutorial;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    protected RecyclerView mRecyclerView;
    protected KartuAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<KartuModel> itemList;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.recycler_view_frag, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(getActivity(),1);
        mRecyclerView.setLayoutManager(mLayoutManager);

        itemList = new ArrayList<KartuModel>();
        itemList.add(new KartuModel("CNBLUE","CNBLUE's Japan Album Puzzle",R.drawable.cnblue));

        mAdapter = new KartuAdapter(itemList);
        mRecyclerView.setAdapter(mAdapter);

        return rootView;
        //return inflater.inflate(R.layout.fragment_profile, container, false);
    }

}
