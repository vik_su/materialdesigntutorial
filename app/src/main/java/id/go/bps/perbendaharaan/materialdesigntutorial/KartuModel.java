package id.go.bps.perbendaharaan.materialdesigntutorial;

/**
 * Created by viktor.suwiyanto on 27/06/2016.
 */
public class KartuModel {
    String judul;
    String deskripsi;
    private int gambar;

    public KartuModel(String judul, String deskripsi, int gambar) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.gambar = gambar;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getGambar() {
        return gambar;
    }

    public void setGambar(int gambar) {
        this.gambar = gambar;
    }
}
