# Tutorial Material Design Android Studio #

Tutorial ini berisi penggunaan material design yang direkomendasikan oleh google untuk membuat tampilan aplikasi android jadi lebih menarik dan interaktif.

### Material Design apa saja yang ada di tutorial ini? ###

* Sliding Tab
* Navigation Drawer
* Cardview - List
* Cardview - Grid
* Search Dialog